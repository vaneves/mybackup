# MyBackup

O **MyBackup** é um Shell Script para fazer backup automaticamente do MySQL no Linux.

## Instalação

Exemplo de instalação e configuração do **MyBackup** no Linux Ubuntu.
Baixe o arquivo `backup.sh` e coloque-o no diretório `/opt/`. Ainda dentro do mesmo diretório, crie uma pasta para salvar os arquivos `/opt/mysql-backup/`. 

## Configuração

Edite o arquivo `backup.sh` e configure as variáveis `DIR`, `DATABASE`, `USE` e `PASSWORD`.

### Crontab

Edite o arquivo `/etc/crontab` e adicione a execução automática do backup.

	25 6	* * *	root	/opt/backup.sh daily
	47 6	* * 7	root	/opt/backup.sh weekly
	52 6	1 * *	root	/opt/backup.sh monthly