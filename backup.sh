#!/bin/sh

NOW=$(date +"%Y-%d-%m")
#sem a barra no final
DIR="/opt/backup-mysql"
DATABASE="database"
USE="user"
PASSWORD="1234"

echo "Criando backup $DIR/$DATABASE-$1-$NOW.sql.gz"

mysqldump -u$USER -p$PASSWORD $DATABASE | gzip > $DIR/$DATABASE-$1-$NOW.sql.gz